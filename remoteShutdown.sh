#!/bin/sh

# Get vSunSpire vmid
VMID=`vim-cmd vmsvc/getallvms | grep vSunSpire | awk '{print $1}'`

# Suspend
vim-cmd vmsvc/power.suspend "$VMID"

# Enter Maintenance Mode
vim-cmd hostsvc/maintenance_mode_enter

# Shutdown Host
poweroff
#!/usr/bin/python

import subprocess
import re
import logging
import datetime
import time


# VMware CLI path to vmcontrol.pl and credentials store
VMCONTROL = "/usr/lib/vmware-vcli/apps/vm/vmcontrol.pl"
CREDSTORE = "/root/.vmware/credstore/vicredentials.xml"

# Hosts to shutdown (DNS or IP)
ESXI_HOSTS = ["esxi3.ds.revolvingsq.net", "esxi2.ds.revolvingsq.net", "esxi1.ds.revolvingsq.net"]

# Excluded Hosts/VMs
EXCLUDED_HOSTS = ["esxi1.ds.revolvingsq.net"]
EXCLUDED_VMS = ["vCenter", "vSunSpire"]

# vCenter VM Name
VCENTER_HOST = "esxi1.ds.revolvingsq.net"
VCENTER_HOSTNAME = "vcsa.ds.revolvingsq.net"
VCENTER_VM = "vCenter"

# Shared Storage Details.
STORAGE_USER = "root"
STORAGE_HOST = "nas.ds.revolvingsq.net"

#SSH key authentication only.
SSH_KEY = "/root/.ssh/id_rsa"

# Script Self Shutdown Host
OWNHOST = "esxi1.ds.revolvingsq.net"

# Logger
logger = logging.getLogger('StormSpire')
log_file = '/var/log/stormspire.log'

log_levels = {'debug': logging.DEBUG,
              'info': logging.INFO,
              'warning': logging.WARNING,
              'error': logging.ERROR,
              'critical': logging.CRITICAL}

level = log_levels.get('debug', logging.NOTSET)
logging.basicConfig(level=level,
                    format='%(asctime)s %(name)-s %(levelname)-s: %(message)s',
                    datefmt='%d %b %Y %H:%M:%S',
                    filename=log_file)


def getHostStatus(host_name):

    try:
        cmd = ['ping', '-c', '1', host_name]
        proc = subprocess.Popen(cmd, shell=False, bufsize=-1, stdin=None,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.communicate()
        result = proc.returncode

        return result == 0

    except BaseException as e:
        logging.debug(e)


def getActiveVMs(esxi_host):

    try:
        results = []

        cmd = ['/usr/bin/esxcli', '--credstore', '%s' % CREDSTORE, '-s', '%s' % VCENTER_HOSTNAME,
               '-h', '%s' % esxi_host, 'vm', 'process', 'list']

        proc = subprocess.Popen(cmd, shell=False, bufsize=-1, stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (output, dummy_error) = proc.communicate()

        for line in output.split('\n'):
            if re.search('Display Name: ', line):
                vmname = line.split()[2]
                if not vmname in EXCLUDED_VMS:
                    results.append(vmname)

        return results

    except BaseException as e:
        logging.debug(e)


def shutdownVM(esxi_host, vmname):

    logging.info("Shutdown %s on %s." % (vmname,esxi_host))
    try:
        cmd = [VMCONTROL, '--credstore', '%s' % CREDSTORE, '--server', '%s' % VCENTER_HOSTNAME,
               '--host', '%s' % esxi_host, '--operation', 'shutdown', '--vmname', '%s' % vmname]

        proc = subprocess.Popen(cmd, shell=False, bufsize=-1, stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (output, dummy_error) = proc.communicate()
        return output
        logging.debug(output)

    except BaseException as e:
        logging.debug(e)


def powerOffVM(esxi_host, vmname):

    logging.error("PowerOff %s on %s." % (vmname,esxi_host))
    try:
        cmd = [VMCONTROL, '--credstore', '%s' % CREDSTORE, '--server', '%s' % VCENTER_HOSTNAME,
               '--host', '%s' % esxi_host, '--operation', 'poweroff', '--vmname', '%s' % vmname]

        proc = subprocess.Popen(cmd, shell=False, bufsize=-1, stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (output, dummy_error) = proc.communicate()
        return output
        logging.debug(output)

    except BaseException as e:
        logging.debug(e)


def suspendVM(host, vm):


    try:
        logging.info("Suspend %s on %s." % (vm, host))
        cmd = [VMCONTROL, '--credstore', '%s' % CREDSTORE, '--server', '%s' % host,
               '--operation', 'suspend', '--vmname', '%s' % vm]
        proc = subprocess.Popen(cmd, shell=False, bufsize=-1, stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (output, dummy_error) = proc.communicate()
        return output

    except BaseException as e:
        logging.debug(e)


def activateMaintenanceMode(esxi_host):

    try:
        logging.info("Enter Maintenance Mode %s." % esxi_host)
        cmd = ['/usr/bin/esxcli', '--credstore', '%s' % CREDSTORE, '-s', '%s' % VCENTER_HOSTNAME,
           '-h', '%s' % esxi_host, 'system', 'maintenanceMode', 'set', '--enable=True']
        proc = subprocess.Popen(cmd, shell=False, bufsize=-1, stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (output, dummy_error) = proc.communicate()
        logging.debug("%s" % output)
        return output

    except BaseException as e:
        logging.debug(e)


def shutdownHost(esxi_host):

    try:
        logging.info("Shutdown %s." % esxi_host)
        cmd = ['/usr/bin/esxcli', '--credstore', '%s' % CREDSTORE, '-s', '%s' % VCENTER_HOSTNAME,
           '-h', '%s' % esxi_host, 'system', 'shutdown', 'poweroff', '-r', 'StormSpire']
        proc = subprocess.Popen(cmd, shell=False, bufsize=-1, stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (output, dummy_error) = proc.communicate()
        logging.debug("%s" % output)
        return output

    except BaseException as e:
        logging.debug(e)


def shutdownStorage(host, user, key):

    timer = 1
    try:
        logging.info("Shutdown storage %s in %s minute(s)." % (host, timer))
        cmd = ['/usr/bin/ssh', '-i', key, '%s@%s' % (user, host), 'shutdown -hP +%s' % timer, '&']
        proc = subprocess.Popen(cmd, shell=False, bufsize=-1, stdin=None,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.poll()
        return None

    except BaseException as e:
        logging.debug(e)


def shutdownMe(host, key):

    try:
        logging.info("Self shutdown triggered for %s." % host)

        cmd = ['ssh', '-i', key, 'root@%s' % host, '/store/remoteshutdown.sh', '&']
        proc = subprocess.Popen(cmd, shell=False, bufsize=-1, stdin=None,
                                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.poll()
        return None

    except BaseException as e:
        logging.debug(e)


def main():

    try:
        if getHostStatus(STORAGE_HOST) is True:
            print "host is up"
        else:
            print "host is down"

        if getHostStatus(esxi3.ds.revolvingsq.net) is True:
            print "host is up"
        else:
            print "host is down"


    except:
        logging.exception("Exception: ")

if __name__ == '__main__':
    main()

__author__ = 'hjlinde'

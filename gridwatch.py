##REQUIRES PyQuery and Requests
##pip install pyquery requests

import requests
from pyquery import PyQuery as pq
import datetime
import re
import os
import logging
import sys

# Disable requests logging functionality.
requests_log = logging.getLogger("requests")
requests_log.addHandler(logging.NullHandler())
requsets_log.propagate = False

# Logger
logger = logging.getLogger('Gridwatch')
log_file = '/var/log/gridwatch.log'

log_levels = {'debug': logging.DEBUG,
              'info': logging.INFO,
              'warning': logging.WARNING,
              'error': logging.ERROR,
              'critical': logging.CRITICAL}

level = log_levels.get('debug', logging.NOTSET)
logging.basicConfig(level=level,
                    format='%(asctime)s %(name)-s %(levelname)-s: %(message)s',
                    datefmt='%d %b %Y %H:%M:%S',
                    filename=log_file)

# Link to your loadshedding schedule
GRIDWATCH_URL = 'http://loadshedding.news24.com/zones/305/calendar';

def GetNextScheduledOutage():
    global GRIDWATCH_URL

    response = requests.get(GRIDWATCH_URL) # Get Page
    html = pq(response.content) # PyQuery to get div for stage time
    nextOutageStr = html('.stage_time').text()

    if not nextOutageStr: # Empty string means no load-shedding scheduled.
        return None
    
    # Regex to strip start time (e.g. "18:00 - 20:30 (Stage 1)" becomes "18:00")
    match = re.search("\d{2}:\d{2}", nextOutageStr)
    startTimeStr = match.group(0)
    nextOutageTime = datetime.datetime.strptime(startTimeStr, "%H:%M").time()
    nextOutage = datetime.datetime.combine(datetime.date.today(), nextOutageTime)
    return nextOutage
    
    
def main():

    nextOutage = GetNextScheduledOutage()

    if not nextOutage is None:
        if datetime.datetime.now() > nextOutage:
            sys.exit()

        delta = nextOutage - datetime.datetime.now()
        secondsUntilOutage = delta.seconds
        minutesUntilOutage = secondsUntilOutage / 60

        if minutesUntilOutage < 30:
            logging.info("Minutes until outage: %d" % minutesUntilOutage)
            
        if minutesUntilOutage < 15 and minutesUntilOutage > -15:
            os.system('/usr/bin/python /root/stormspire/stormspire.py')
            logging.info('Shutting systems down due to loadshedding.')


if __name__ == '__main__':
    main()

__author__ = 'hjlinde'